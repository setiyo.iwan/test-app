<?php
    class Order_model extends CI_Model{
    	public $table = "order";

        function __construct() {
            parent::__construct();     
        }

        function filter_data($filter){
        	if($filter['filter_keyword']){
        		$keyword = strtolower($filter['filter_keyword']);
        		$this->db->where("lower(nama) like '%{$keyword}%'");
        	}
        }
		
		function get_all_data($limit='', $start='', $filter=''){
			$this->db->select('*');
			$this->db->from($this->table);
			if($filter) $this->filter_data($filter);

			if($limit) $this->db->limit($limit, $start);
			
			return $this->db->get()->result();

		}

		function count_data($filter){
			$this->db->select('*');
			$this->db->from($this->table);
			
			if($filter) $this->filter_data($filter);

			return $this->db->get()->num_rows();
		}

		function get_data_detail($id){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('id', $id);

			return $this->db->get()->row();
		}

		function insert($data){
			return ($this->db->insert($this->table, $data));
		}

		function update($id, $data){
			$this->db->where('id', $id);
			return ($this->db->update($this->table, $data));
		}

		function delete($id){
			$this->db->where('id', $id);
			return ($this->db->delete($this->table));
		}

		function gel_today_last_order(){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('tanggal', date('Y-m-d'));
			$this->db->order_by('id', 'DESC');
			$last_order = $this->db->get()->row();
			$kode_split = explode('-', $last_order->kode);
			$kode = intval($kode_split[2]) + 1;
			return $kode;
		}
	}