<?php
    class User_model extends CI_Model{
    	public $table = "user";

        function __construct() {
            parent::__construct();     
        }

        function login($username, $password){
			$password = md5($password);
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$data = $this->db->get()->result();
			if(count($data)>0){
				$hasil['username'] 		= $data[0]->username;
				$hasil['nama'] 			= $data[0]->nama;
				$hasil['id']			= $data[0]->id;
				return($hasil);
			}else{
				return(false);
			}
		}

		function get_data($id=null,$type=null,$limit=null, $start=null){
			$this->db->select('a.*,b.nama as nama_grup');
			$this->db->from($this->table.' a');
			$this->db->join('user_grup b','a.id_grup=b.id');
			$this->db->order_by('a.id');

			if($limit!=0){
				$this->db->limit($limit, $start);
			}
			if($id!=null) $this->db->where('a.id',$id);
			if($type!=null)$data = $this->db->get()->$type(); else $data = $this->db->get()->row_array();
			return $data;
		}
    }