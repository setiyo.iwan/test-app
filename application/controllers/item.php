<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends MY_Controller {

	function __construct() 
	{
        parent::__construct();
        if($this->session->userdata('username')==''){
			redirect('home');
		}
		$this->load->model('item_model', 'Item');
		$this->load->model('kategori_model', 'Kategori');
    }

    public function index($page=null)
	{	
		$data['filter_keyword'] = '';
		if($this->session->userdata('keyword_kategori')){
			$data['filter_keyword'] = $this->session->userdata('keyword_kategori');
		}

		$config['base_url'] = base_url()."item/index";
		$config['total_rows'] = $this->Item->count_data($data);
		$config['per_page'] = 10;

		$this->pagination->initialize($config); 

		$data['pagination'] =  $this->pagination->create_links();
		
		$data['title'] = 'Item';
		$data['main'] = 'item/home';

		$data['total_data'] = $config['total_rows'];
		$data['data_list'] = $this->Item->get_all_data($config['per_page'],$page, $data);
		$this->load->view('admin/header',$data);
	}

	function add(){
		$data['title'] 	= 'Form Add';

		$data['form_action'] = 'item/add_post';
		$data['main'] 	= 'item/add';
		$data['kategori'] = $this->Kategori->get_all_data();

		$this->load->view('admin/header', $data);
	}

	function add_post(){
		$data = array(
			'nama' => $this->input->post('nama'),
			'id_kategori' => $this->input->post('id_kategori'),
			'status' => $this->input->post('status'),
			);

		$this->Item->insert($data);
		redirect('item');
	}

	function edit($id=''){
		$data['title'] = 'Form Edit';

		$data['form_action'] = 'item/edit_post/'.$id;
		$data['data'] = $this->Item->get_data_detail($id);
		$data['main'] = 'item/edit';
		$data['kategori'] = $this->Kategori->get_all_data();

		$this->load->view('admin/header', $data);
	}

	function edit_post($id=''){
		$data = array(
			'nama' => $this->input->post('nama'),
			'id_kategori' => $this->input->post('id_kategori'),
			'status' => $this->input->post('status'),
			);

		$this->Item->update($id, $data);
		redirect('item');
	}

	function delete($id=''){
		$this->Item->delete($id);
		redirect('item');
	}

	function filter(){
		$this->session->set_userdata('keyword_kategori', $this->input->post('keyword'));
		redirect('item');
	}

	function clear(){
		$this->session->unset_userdata('keyword_kategori');

		redirect('item');
	}

}