<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends MY_Controller {
	
	function __construct() {
        parent::__construct();
        if($this->session->userdata('username')==''){
			redirect('home');
		}
		$this->load->model('order_model', 'Order');
		$this->load->model('item_model', 'Item');
    }

	public function index($page=null)
	{	
		$data['filter_keyword'] = '';
		if($this->session->userdata('keyword_kategori')){
			$data['filter_keyword'] = $this->session->userdata('keyword_kategori');
		}

		$config['base_url'] = base_url()."order/index";
		$config['total_rows'] = $this->Order->count_data($data);
		$config['per_page'] = 10;

		$this->pagination->initialize($config); 

		$data['pagination'] =  $this->pagination->create_links();
		
		$data['title'] = 'Order';
		$data['main'] = 'order/home';

		$data['total_data'] = $config['total_rows'];
		$data['data_list'] = $this->Order->get_all_data($config['per_page'],$page, $data);
		$this->load->view('admin/header',$data);
	}

	function add(){
		$data['title'] 	= 'Form Add';

		$data['form_action'] = 'order/add_post';
		$data['main'] 	= 'order/add';
		$data['item'] 	= $this->Item->get_all_data('', '', '', array('status' => 1));
		$data['kode'] 	= $this->Order->gel_today_last_order();
		$this->load->view('admin/header', $data);
	}

	function add_post(){
		$data = array(
			'nama' => $this->input->post('nama'),
			'nomor_meja' => $this->input->post('nomor_meja'),
			'status' => $this->input->post('status'),
			'id_item' => $this->input->post('id_item'),
			'kode' => $this->input->post('kode'),
			'tanggal' => date('Y-m-d'),
			'id_user' => $this->session->userdata('id'),
			);

		$this->Order->insert($data);
		redirect('order');
	}

	function edit($id=''){
		$data['title'] = 'Form Edit';

		$data['form_action'] = 'order/edit_post/'.$id;
		$data['data'] = $this->Order->get_data_detail($id);
		$data['main'] = 'order/edit';
		$data['item'] = $this->Item->get_all_data('', '', '', array('status' => 1));

		$this->load->view('admin/header', $data);
	}

	function edit_post($id=''){
		$data = array(
			'nama' => $this->input->post('nama'),
			'nomor_meja' => $this->input->post('nomor_meja'),
			'status' => $this->input->post('status'),
			'id_item' => $this->input->post('id_item'),
			'kode' => $this->input->post('kode'),
			'id_user' => $this->session->userdata('id'),
			);

		$this->Order->update($id, $data);
		redirect('order');
	}

	function delete($id=''){
		$this->Order->delete($id);
		redirect('order');
	}

	function filter(){
		$this->session->set_userdata('keyword_kategori', $this->input->post('keyword'));
		redirect('order');
	}

	function clear(){
		$this->session->unset_userdata('keyword_kategori');

		redirect('order');
	}

}