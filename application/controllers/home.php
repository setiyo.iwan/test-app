<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	function __construct() {
        parent::__construct(); 
    	if($this->session->userdata('username')!=''){
			redirect('order');
		}
		$this->load->model('user_model', 'User');
    }

	public function index()
	{
		$data['title'] = 'Welcome to App';
		
		$this->load->view('home/login',$data);
	}

	function do_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data_user = $this->User->login($username, $password);
		if($data_user !== false){
			$this->session->set_userdata('nama', $data_user['nama']);
			$this->session->set_userdata('username', $data_user['username']);
			$this->session->set_userdata('id', $data_user['id']);
			redirect('order');
		}else{	
			$this->session->set_userdata('error_login', 'Login gagal, username atau password salah.');
			redirect('home');
		}
	}

}