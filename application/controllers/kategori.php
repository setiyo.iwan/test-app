<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends MY_Controller {

	function __construct() 
	{
        parent::__construct();
        if($this->session->userdata('username')==''){
			redirect('home');
		}
		$this->load->model('kategori_model', 'Kategori');
    }

    public function index($page=null)
	{	
		$data['filter_keyword'] = '';
		if($this->session->userdata('keyword_kategori')){
			$data['filter_keyword'] = $this->session->userdata('keyword_kategori');
		}

		$config['base_url'] = base_url()."kategori/index";
		$config['total_rows'] = $this->Kategori->count_data($data);
		$config['per_page'] = 10;

		$this->pagination->initialize($config); 

		$data['pagination'] =  $this->pagination->create_links();
		
		$data['title'] = 'Kategori';
		$data['main'] = 'kategori/home';

		$data['total_data'] = $config['total_rows'];
		$data['data_list'] = $this->Kategori->get_all_data($config['per_page'],$page, $data);
		$this->load->view('admin/header',$data);
	}

	function add(){
		$data['title'] 	= 'Form Add';

		$data['form_action'] = 'kategori/add_post';
		$data['main'] 	= 'kategori/add';

		$this->load->view('admin/header', $data);
	}

	function add_post(){
		$data = array(
			'nama' => $this->input->post('nama'),
			);

		$this->Kategori->insert($data);
		redirect('kategori');
	}

	function edit($id=''){
		$data['title'] = 'Form Edit';

		$data['form_action'] = 'kategori/edit_post/'.$id;
		$data['data'] = $this->Kategori->get_data_detail($id);
		$data['main'] = 'kategori/edit';

		$this->load->view('admin/header', $data);
	}

	function edit_post($id=''){
		$data = array(
			'nama' => $this->input->post('nama'),
			);

		$this->Kategori->update($id, $data);
		redirect('kategori');
	}

	function delete($id=''){
		$this->Kategori->delete($id);
		redirect('kategori');
	}

	function filter(){
		$this->session->set_userdata('keyword_kategori', $this->input->post('keyword'));
		redirect('kategori');
	}

	function clear(){
		$this->session->unset_userdata('keyword_kategori');

		redirect('kategori');
	}

}