<?php
	class Logout extends MY_Controller{
		function __construct(){
			parent::__construct();	
			$this->session->unset_userdata('username');
			redirect('home');
		}	
	}
?>