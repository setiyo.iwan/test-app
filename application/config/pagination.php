<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Here Config for Pagination */

/*First link tag*/
$config['first_link'] = '&laquo;';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

/*Last link tag*/
$config['last_link'] = '&raquo;';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

/*Next link tag*/
$config['next_link'] = '&gt;';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';

/*Previous link tag*/
$config['prev_link'] = '&lt;';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';

/*Number link tag*/
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

/*Current link tag*/
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';