<div id="page-wrapper">
            <div class="row">
                 <!-- page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Add Order</h1>
                </div>
                <!--end page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Add
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?=base_url().$form_action?>" method="POST">
                                        <div class="form-group">
                                            <label>Kode</label>
                                            <input name="kode" class="form-control" value="<?='ERP-'.date("dmY").'-00'.$kode; ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pelanggan</label>
                                            <input name="nama" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Meja</label>
                                            <input name="nomor_meja" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Item</label>
                                            <select name="id_item" class="form-control">
                                                <?php foreach($item as $val){ ?>
                                                    <option value="<?=($val->id);?>"><?=$val->nama;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn">Reset</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        </div>