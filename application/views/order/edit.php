<div id="page-wrapper">
            <div class="row">
                 <!-- page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Order</h1>
                </div>
                <!--end page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Edit
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?=base_url().$form_action?>" method="POST">
                                        <div class="form-group">
                                            <label>Kode</label>
                                            <input name="kode" class="form-control" value="<?=$data->kode?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pelanggan</label>
                                            <input name="nama" class="form-control" value="<?=$data->nama?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Meja</label>
                                            <input name="nomor_meja" class="form-control" value="<?=$data->nomor_meja?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Item</label>
                                            <select name="id_item" class="form-control">
                                                <?php foreach($item as $val){ ?>
                                                    <option value="<?=($val->id);?>" <?=($data->id_item == $val->id ? 'selected' : '');?>><?=$val->nama;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1" <?=$data->status == 1 ? "selected" : ""; ?>>Aktif</option>
                                                <option value="0" <?=$data->status == 0 ? "selected" : ""; ?>>Tidak Aktif</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn">Reset</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        </div>