<div id="page-wrapper">
            <div class="row">
                 <!-- page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Add Item</h1>
                </div>
                <!--end page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Add
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?=base_url().$form_action?>" method="POST">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="id_kategori" class="form-control">
                                                <?php foreach($kategori as $val){ ?>
                                                    <option value="<?=($val->id);?>"><?=$val->nama;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Ready</option>
                                                <option value="0">Not Ready</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn">Reset</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        </div>