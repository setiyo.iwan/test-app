<div id="page-wrapper">
            <div class="row">
                 <!-- page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Item</h1>
                </div>
                <!--end page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Edit
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?=base_url().$form_action?>" method="POST">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" class="form-control" value="<?=$data->nama?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="id_kategori" class="form-control">
                                                <?php foreach($kategori as $val){ ?>
                                                    <option value="<?=($val->id);?>" <?=($data->id_kategori == $val->id ? 'selected' : '');?>><?=$val->nama;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1" <?=$data->status == 1 ? "selected" : ""; ?>>Ready</option>
                                                <option value="0" <?=$data->status == 0 ? "selected" : ""; ?>>Not Ready</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn">Reset</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        </div>