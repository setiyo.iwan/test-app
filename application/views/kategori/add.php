<div id="page-wrapper">
    <div class="row">
         <!-- page header -->
        <div class="col-lg-12">
            <h1 class="page-header">Add Kategori Item</h1>
        </div>
        <!--end page header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form Add
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" action="<?=base_url().$form_action?>" method="POST">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input name="nama" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
             <!-- End Form Elements -->
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(function(){
        /* jQuery Validation */
        $('form').validate();
        $.validator.messages.required = 'Field tidak boleh kosong!';

        /* notification */
        //notification('success','Success Dab!');
        /* 
        notification('type','message');
        type ada 4 macam : success, warning, info, error
        */
    });
</script>