        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Kategori Item</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?=base_url()?>kategori/add" class="btn btn-success btn-sm">Tambah Data</a>
                            <div class="pull-right">
                                <form action="<?=base_url()?>kategori/filter" method="POST">
                                    <input type="text" class="form-control input-sm" name="keyword" value="<?=$filter_keyword?>" placeHolder="Search....">
                                </form>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="120">Aksi</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data_list as $data):?>
                                        <tr>
                                            <td class="text-center">
                                                <a href="<?=base_url()?>kategori/edit/<?=$data->id?>" class="fa fa-pencil" title="Edit"></a>
                                                <a href="<?=base_url()?>kategori/delete/<?=$data->id?>" class="fa fa-trash-o" title="Hapus"></a>
                                            </td>
                                            <td><?=$data->nama?></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pull-left">
                                Menampilkan <b><?=count($data_list)?></b> dari <b><?=$total_data?></b> data
                            </div>
                            <div class="pull-right">
                                <ul class="pagination">
                                    <?php if($pagination){?>
                                        <?=$pagination?>
                                    <?php }else{?>
                                        <li><a href="#">&laquo;</a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">&raquo;</a></li>
                                    <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>