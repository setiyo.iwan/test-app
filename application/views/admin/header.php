<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=isset($title) ? $title : "App"?></title>
    <link href="<?=base_url()?>assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/main-style.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />

    <script src="<?=base_url()?>assets/plugins/jquery-1.10.2.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap/bootstrap.min.js"></script>
   </head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="<?=base_url()?>assets/img/logo.png">
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a href="<?=base_url()?>logout">Logout</a>
                </li>
            </ul>
        </nav>
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="<?=base_url()?>assets/img/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div><strong>Administrator</strong></div>
                                <div class="user-text-online">
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="sidebar-search">
                    </li>
                    <li>
                        <a href="<?=base_url()?>order"><i class="fa fa-user fa-fw"></i>Order</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Item<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?=base_url()?>item"><i class="fa fa-user fa-fw"></i>Item</a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>kategori"><i class="fa fa-user fa-fw"></i>Kategori</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                </ul>
            </div>
        </nav>

        <?=$this->load->view($main);?>

    </div>

    <script src="<?=base_url()?>assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=base_url()?>assets/plugins/pace/pace.js"></script>
    <script src="<?=base_url()?>assets/scripts/siminta.js"></script>
    <script src="<?=base_url()?>assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/morris/morris.js"></script>
    <script src="<?=base_url()?>assets/scripts/dashboard-demo.js"></script>

</body>

</html>
