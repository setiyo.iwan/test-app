<?php
	class MY_Controller extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		public function IsLogin(){
			if(!$this->session->userdata('username')){
				return false;
			}
			return true;
		}
		
		public function IsAdmin(){
			if($this->session->userdata('id_grup') == 1){
				return true;
			}
		}
		
		public function IsOperator(){
			if($this->session->userdata('id_grup') == 2){
				return true;
			}
		}
		
	}
?>